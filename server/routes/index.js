const express = require('express');
const usersRoutes = require('./users');
const requestRoutes = require('./requests');

const apiRouter = express.Router();

apiRouter.use('/users', usersRoutes);
apiRouter.use('/requests', requestRoutes);

module.exports = apiRouter;
