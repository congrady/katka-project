const express = require('express');
const { RequestModel } = require('../models');
const { isEntityNotFoundWhileUpdating } = require('../utils');

const router = express.Router();

// create
router.post('/', (req, res) => {
  const data = req.body;
  RequestModel.create(data)
    .then(result => {
      res.status(201).json({ message: 'success', id: result.id });
    })
    .catch(error => {
      res.status(422).json({ error });
    });
});

// update
router.put('/:id', (req, res) => {
  const data = req.body;
  RequestModel.update(data, { where: { id: req.params.id } })
    .then(result => {
      if (isEntityNotFoundWhileUpdating(result)) throw 'Not found';
      res.status(202).json({ message: 'success', id: result.id });
    })
    .catch(error => {
      res.status(422).json({ error });
    });
});

// delete
router.delete('/:id', (req, res) => {
  RequestModel.destroy({ where: { id: req.params.id } })
    .then(result => {
      res.status(200).json({ message: 'success', id: result.id });
    })
    .catch(error => {
      res.status(422).json({ error });
    });
});

// get all
router.get('/', (req, res) => {
  RequestModel.all({ include: [{ all: true }] })
    .then(result => {
      res.status(200).json({ message: 'success', data: result });
    })
    .catch(error => {
      res.status(404).json({ error });
    });
});

// get by id
router.get('/:id', (req, res) => {
  RequestModel.findById(req.params.id, { include: [{ all: true }] })
    .then(result => {
      if (!result) throw 'Not found';
      res.status(200).json({ message: 'success', data: result });
    })
    .catch(error => {
      res.status(404).json({ error });
    });
});

module.exports = router;
