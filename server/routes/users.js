const express = require('express');
const { UserModel } = require('../models');
const { isEntityNotFoundWhileUpdating } = require('../utils');

const router = express.Router();

// create
router.post('/', (req, res) => {
  const data = req.body;
  UserModel.create(data)
    .then(result => {
      res.status(201).json({ message: 'success', id: result.id });
    })
    .catch(error => {
      res.status(422).json({ error });
    });
});

// update
router.put('/:id', (req, res) => {
  const data = req.body;
  UserModel.update(data, { where: { id: req.params.id } })
    .then(result => {
      if (isEntityNotFoundWhileUpdating(result)) throw 'Not found';
      res.status(202).json({ message: 'success', id: result.id });
    })
    .catch(error => {
      res.status(422).json({ error });
    });
});

// delete
router.delete('/:id', (req, res) => {
  UserModel.destroy({ where: { id: req.params.id } })
    .then(result => {
      res.status(200).json({ message: 'success', id: result.id });
    })
    .catch(error => {
      res.status(422).json({ error });
    });
});

// get all
router.get('/', (req, res) => {
  UserModel.all({ include: [{ all: true }] })
    .then(result => {
      res.status(200).json({ message: 'success', data: result });
    })
    .catch(error => {
      res.status(404).json({ error });
    });
});

// get by id
router.get('/:id', (req, res) => {
  UserModel.findById(req.params.id, { include: [{ all: true }] })
    .then(result => {
      if (!result) throw 'Not found';
      res.status(200).json({ message: 'success', data: result });
    })
    .catch(error => {
      res.status(404).json({ error });
    });
});

module.exports = router;
