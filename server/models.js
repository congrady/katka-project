const sequelize = require('./sequelize');
const { STRING } = require('sequelize');

const UserModel = sequelize.define('user', {
  username: {
    type: STRING,
    unique: true,
    allowNull: false
  },
  password: {
    type: STRING,
    allowNull: false
  }
});

const RequestModel = sequelize.define('request', {
  method: {
    type: STRING,
    allowNull: false
  },
  endpoint: {
    type: STRING,
    allowNull: false
  }
});

UserModel.hasMany(RequestModel, { as: 'requests', foreignKey: { allowNull: false }, onDelete: 'CASCADE' });
RequestModel.belongsTo(UserModel, {
  allowNull: false,
  foreignKey: { allowNull: false },
  onDelete: 'CASCADE'
});

module.exports = { UserModel, RequestModel };
