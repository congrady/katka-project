const Sequelize = require('sequelize');

const isDev = process.env.NODE_ENV === 'development';

module.exports = new Sequelize(
  process.env.DB_NAME || 'data-visualizer-db',
  process.env.DB_USER || 'root',
  isDev ? '' : 'c1965a84f782968f523096494d95d1fba523dbcac2db3b52',
  {
    host: 'localhost',
    dialect: 'mysql'
  }
);
