### Server

* server IP: 159.89.29.254 ("ssh root@159.89.29.254")

### Dev setup
-- install mysql database (on windows "choco install mysql")
-- create database with name "data-visualizer-db" (on windows, "mysqladmin -u root -p create data-visualizer-db")

### Deploying
ssh root@159.89.29.254
cd node-app
git pull
