import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

const renderApp = () => {
  const App = require('./components/App').default;
  render(
    <BrowserRouter>
      <App />
    </BrowserRouter>,
    document.getElementById('app')
  );
};

renderApp();

if (module.hot) {
  module.hot.accept('./components/App', renderApp);
}
