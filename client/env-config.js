module.exports = {
  NODE_ENV: JSON.stringify(process.env.NODE_ENV),
  DEFAULT_LOCALE: process.env.DEFAULT_LOCALE || 'en'
};
