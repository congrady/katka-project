import { store } from 'react-easy-state';

export default store({
  counter: 0,
  increment() {
    this.counter++;
  },
  decrement() {
    this.counter--;
  }
});
