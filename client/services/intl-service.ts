import messages from '../locale/messages';

export let currentUserLanguage = localStorage.getItem('selectedLocale') || process.env.DEFAULT_LOCALE; // eslint-disable-line
export const changeLocale = (newLocale: string): void => {
  currentUserLanguage = newLocale;
  localStorage.setItem('selectedLocale', newLocale);
};

let cache = { _userLang: currentUserLanguage };
export function trans(msg: string): string | Function {
  if (cache._userLang != currentUserLanguage) {
    cache = { _userLang: currentUserLanguage };
  }
  const cachedResult = cache[msg];
  if (cachedResult) {
    return cachedResult;
  }
  const res = messages[currentUserLanguage][msg];
  if (!res) {
    if (cache[`${msg}_NOT_FOUND`]) return msg;
    console.warn(`Missing translation for ${msg}`);
    cache[`${msg}_NOT_FOUND`] = true;
    return msg;
  }
  cache[msg] = res;
  return res;
}

export const localeDate = (date: Date): string => new Date(date).toLocaleDateString(currentUserLanguage);
export const localeTime = (date: Date): string => new Date(date).toLocaleTimeString(currentUserLanguage);
export const localeDateTime = (date: Date): string => new Date(date).toLocaleString(currentUserLanguage);
export const ISOString = (date: Date): string => new Date(date).toISOString();
