import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './components/Home/Home';

export default function Routes() {
  return (
    <Switch>
      <Route exact path="(/home|)" component={Home} />
    </Switch>
  );
}
