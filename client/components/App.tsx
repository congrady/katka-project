import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Routes from '../routes';
import { changeLocale } from 'intl';
import UIStore from '../stores/UI-store';
import { view } from 'react-easy-state';

@withRouter
@view
export default class App extends Component {
  componentDidCatch(error, info) {
    console.error(info);
  }

  render() {
    return (
      <>
        <div>
          counter: <span>{UIStore.counter}</span>
        </div>
        <button
          onClick={() => {
            changeLocale('sk');
            this.forceUpdate();
          }}
        >
          Slovensky
        </button>
        <button
          onClick={() => {
            changeLocale('en');
            this.forceUpdate();
          }}
        >
          English
        </button>
        <button
          onClick={() => {
            UIStore.increment();
          }}
        >
          +
        </button>
        <button
          onClick={() => {
            UIStore.decrement();
          }}
        >
          -
        </button>
        <Routes />
      </>
    );
  }
}
