import React from 'react';
import { trans } from 'intl';
import { style } from 'typestyle';
import { view } from 'react-easy-state';

function Home() {
  return (
    <>
      <div className={style({ marginBottom: '15px' })}>{trans('home')}</div>
    </>
  );
}

export default view(Home);
